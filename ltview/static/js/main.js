
$(function() {
    console.log("hello");

    var uri = 'ws://' + window.location.host + '/ws';
    ws = new WebSocket(uri);

    ws.onopen = function(e) {
        console.log("connection established");
    }

    var newimage = function(e) {
        console.log("new image");
        var png = "data:image/png;base64," + e.data;
        // $("#image").attr('src', png);
        ws.send("hi from client");
        console.log(e.data);
        data = png;
    };    
    
    ws.onmessage = function(e) {
        console.log("message: " + e.data); 
    };

    ws.onerror = function(e) {
        console.log("websocket error: ", e);
    };

    ws.onclose = function() {
        console.log("socket closed");
    };




    $("#button").click(function() {
        console.log("button clicked");
    });


});


$.ajaxSetup ({
    // Disable caching of AJAX responses
    'cache': false,
});