from sanic import Sanic
from sanic.response import json, text, raw
from sanic.log import logger
from sanic.websocket import WebSocketProtocol

import jinja2
import jinja2_sanic

import asyncio

import base64

import sys
import os

# from sanic.views import HTTPMethodView
# from sanic.exceptions import ServerError

LTVIEW_DIR = os.path.dirname(__file__)
def ltview_dir(d):
    return os.path.join(LTVIEW_DIR, d)

app = Sanic('hello')

app.static("/static", ltview_dir("./static"))
app.static("js", ltview_dir("./static/js"))
app.static("css", ltview_dir("./static/css"))
app.static("/icon.png", ltview_dir("./static/icon.png"))
app.static("/favicon.ico", ltview_dir("./static/favicon.ico"))


data = {
    'image': b'',
    'image64': b'',

}
image_event = None




@app.listener('before_server_start')
def init(sanic, loop):
    # have to create Event after sanic main loop starts
    # so the event can find the loop.
    global image_event
    image_event = asyncio.Event()
    image_event.clear()


jinja2_sanic.setup(
    app,
    loader=jinja2.FileSystemLoader(ltview_dir("templates")),
)


@app.route('/json')
async  def test(request):
    logger.info('log')
    return json({
        "text": "hello world",
        "args": request.args,
        "raw_args": request.raw_args,
        "url": request.url,
        "query_string": request.query_string,
        "request": request,
    })

@app.route("/image.png")
async def image(request):
    return raw(
        data['image'],
        headers={
            "Content-Type": "image/png",
        },
    )

@app.route("/")
@jinja2_sanic.template("index.jinja2.html")
async def index(request):
    return {
        'body': "hello world"
    }


class Clients(object):
    def __init__(self):
        self.cs = []

    def register(self, ws):
        self.cs.append(ws)

    def unregister(self, ws):
        self.cs.remove(ws)

    async def send(self, m):
        if self.cs:
            await asyncio.wait([c.send(m) for c in self.cs])
            

clients = Clients()
        
@app.websocket("/ws")
async def websocket(request, ws):
    await ws.send("hi from server")

    clients.register(ws)

    try:
        async for m in ws:
            print("got message '%s'" % m)
            sys.stdout.flush()
            await clients.send("up")
    finally:
        clients.unregister(ws)



@app.route("/upload", methods=['POST'])
async def upload(request):
    print("got upload")

    f = request.files["image"][0]
    data['image'] = f.body
    data['image64'] = base64.b64encode(f.body)

    image_event.set()
    image_event.clear()

    return text("ok")


def main():
    app.run(host="0.0.0.0", port=6574, debug=True, access_log=True)# ,protocol=WebSocketProtocol

if __name__ == "__main__":
    main()
    

