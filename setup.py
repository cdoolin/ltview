#!/usr/bin/env python

from distutils.core import setup

setup(name='ltview',
      version='0.1',
      author='Callum Doolin',
      author_email='doolin@ualberta.ca',
      packages=['ltview'],
      scripts=['scripts/ltview'],
      py_modules = ['ltdata'],
     )