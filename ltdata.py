import pickle
import bz2
import asyncio
import websockets
import base64
import io
import json

import ipywidgets
from IPython.display import HTML


import logging
logger = logging.getLogger("ltview")

# scenv = {'actions': {}}


class SocketClient(object):
    # _actions = scenv['actions']
    
    def __init__(self, server="localhost", start=False):
        self._socket = None
        self._stdout = ipywidgets.Output(layout={'border': '1px solid #404040'})
        self.server = server

        if server is not None and start:
            self.start_websocket_loop()

    def print(self, s):
        with self._stdout:
            print(s)


        
    # decoration @action adds these class methods
    # as actions that can be called through websockets
    _actions = []    
    
#     def action(func):
#         ''' @action decorator '''
#         scenv['actions'][func.__name__] = func
# #         MyType._actions.append(func)
# #         _actions.append(func)
#         return func

#     def call_action(self, action, arg=None):
#         try:
#             scenv['actions'][action](self, arg)
#         except KeyError as e:
#             raise RuntimeError(f"unable to run action \"{action}\"")
        

#     @action
        
#     @action


    def connected(self, msg=None):
        print("connected")
        
    def disconnected(self, msg=None):
        print("disconnected")
        
        


    async def _send_msg(self, action, **kwargs):
        msg = {'action': action, **kwargs}

        if self._socket is not None:
            await self._socket.send(json.dumps(msg))
        else:
            self.print("did not send msg because no websocket connection")
    
            
    def send(self, m):
        # iffy!
        for i in self._socket.send(m):
            pass
        

    async def connect_loop(self):
        while True:
            try:
                async with websockets.connect("ws://%s:6574/ws" % self.server) as ws:
                    self.print("connected to websocket?")
                    await self.socket_loop(ws)

            except OSError:
                self.print("could not connect to ws")

            self.print("try to connect in 10s")
            await asyncio.sleep(10)


    async def socket_loop(self, ws):
        self._socket = ws
        self.connected('connected')

        try:
            async for m in ws:
                self.print("got %s" % m)
                try:
                    m = json.loads(m)
                    self.call_action(m['action'], m)
                except:
                    self.print("unable to do_message %s" % m)


        finally:
            self.print("socket finally")
            self._socket = None
            self.disconnected()



    def start_websocket_loop(self):
        loop = asyncio.get_event_loop()
        if not loop.is_running():
            print("expect event loop to be already running")
            return
        
        loop.create_task(self.connect_loop())
        self.print("started websocket loop")

    start = start_websocket_loop









class DataSet(SocketClient):
    # acts as a list. list of fitobjects to fit.
    # uploads fit status and provides general fit interface to ltview server
    # _finder_task = None

    def __init__(self, DataClass, server="localhost", start=True, name=None):
        
        # ltview server to update to
        SocketClient.__init__(self, server=server, start=start)
        self.all = []
        
        self.Data = DataClass
        
        # _finder is a function that returns a list of identifying objects used for the fit.
        # _finder is set with the decorator @finder().
        # finder should always return the complete list of identifyers.  new identifiers added to that
        # list will be loaded and operated on.
        self._finder =  None
        

        self._figure_funcs = {}
        self._figure_data = {}

        self._finder = None
        # event that is triggered everytime an update happens
        self._update_event = asyncio.Event()
        # event that can be triggered to cause an immediate update to happen
        self._signal_update_event = asyncio.Event()
        self._new_data_to_plot = False

        self._finder_go = True
        self._finder_task = None
        self._finder_every = 30

        self._loop = asyncio.get_event_loop()
        if not self._loop.is_running():
            raise RuntimeError("expect ayncio event loop to be already running")

        if name is not None:
            self._name = name
        else:
            self._name = "unamed"

        self._figout = ipywidgets.Output(layout={'border': '1px solid #004040'})

        if start:
            self.start()

    def __iter__(self):
        for i in self.all:
            yield i
            
    def __len__(self):
        return len(self.all)

    def __getitem__(self, ii):
        return self.all[ii]

    def __delitem__(self, ii):
        del self.all[ii]

    def __setitem__(self, ii, val):
        self.all[ii] = val

                
    def find_next(self, item):
        i = self.all.index(item)
        if (i + 1) < len(self.all):
            return self.all[i + 1]
        else:
            return None
        
    def find_last(self, item):
        i = self.all.index(item)
        if i > 0:
            return self.all[i - 1]
        else:
            return None
        



    def dumplist(self):
        return [a.to_dict() for a in self.all]
    
    def fromlist(self, lis):
        all = []
        for dic in lis:
            a = self.Data(dataset=self, **dic)
            all.append(a)

        self.all = all


    def savebz2(self, fname):
        with bz2.BZ2File(fname, 'wb') as f:
            pickle.dump(self.dumplist(), f)

    def loadbz2(self, fname):
        with bz2.open(fname, 'rb') as f:
            lis = pickle.load(f)
        self.fromlist(lis)


    def save(self, fname):
        with open(fname, 'wb') as f:
            pickle.dump(self.dumplist(), f)

    def load(self, fname):
        with open(fname, 'rb') as f:
            lis = pickle.load(f)
        self.fromlist(lis)


    def fit_all(self, **kwargs):
        for fd in self.all:
            if fd.fit_status != 10:
                fd.fit(**kwargs)

    def upgrade_model(self, NewClass):
        self.Data = NewClass
        newall = []
        for a in self.all:
            newall.append(
                NewClass(dataset=self, **a.to_dict())
            )

        self.all = newall




    async def await_update(self):
        await self._update_event.wait()

    def wait_update(self):
        if self._finder_task is not None:
            self._loop.run_until_complete(self.await_update())

    def finder(self, update=True, run=True, every=30):
        def decorator(func):
            self._finder = func
            self._finder_every = every
            self.update()

            return func

        return decorator

    
    def figure(self, name=None, other=2):
        def decorator(func):
            
            if name is None:
                thisname = func.__name__
            else:
                thisname = name

            self._figure_funcs[thisname] = func
            func(self)

            self._new_data_to_plot = True
            self._signal_update_event.set()
            self._signal_update_event.clear()
            return func

        return decorator

    def start(self):
        loop = asyncio.get_event_loop()
        if not loop.is_running():
            print("expect event loop to be already running")
            return

        if self._finder_task is not None:
            if not self._finder_task.done():
                print("finder loop is already running!")
                return
            else:
                self._finder_task = None
        
        self._finder_go = True
        self._finder_task = loop.create_task(self._finder_loop())

        def done_callback(future):
            self.print("finder loop stopped.")
            exc = self._finder_task.exception()
            if exc is not None:
                self.print("exception:")
                self.print(exc)

        self._finder_task.add_done_callback(done_callback)
        self.print("started finder loop")

    def stop(self):
        self._finder_go = False


    async def _finder_loop(self):
        """
        main loop for updating. started when dataset it created, or with start method
        """

        while self._finder_go:
            if self._finder is not None:
                await self._update_finder()
                self.print("update %d" % len(self.all))

            try:
                await asyncio.wait_for(self._signal_update_event.wait(), timeout=self._finder_every)
            except asyncio.TimeoutError:
                pass
    
    def update(self, force_redraw=False):
        if force_redraw:
            self._new_data_to_plot = True
            
        self._signal_update_event.set()
        self._signal_update_event.clear()
        



    async def _update_finder(self):
        if self._finder is not None:
            new_idens = self._finder()
        else:
            raise RuntimeError("need a finder")
        
        idens = [d.iden for d in self.all]
        if idens != new_idens[:len(self.all)]:
            raise RuntimeError("expected id list to be same until new values")
        
        self._new_data_to_plot |= len(new_idens) > len(idens)

        for i, iden in enumerate(new_idens[len(self.all):]):
            self.all.append(self.Data(iden, self))
            if i % 10 == 0:
                await asyncio.sleep(0.001)

        if self._new_data_to_plot:
            await self._new_data()
            self._new_data_to_plot = False
        
        self._update_event.set()
        self._update_event.clear()


    async def _new_data(self):
        self.print("new data")
        await self.render_figures()

        await self._send_msg(
            action = 'update',
            dataset_name = self._name,
            n = len(self.all),
            figures = self._figure_data,
        )


    async def render_figures(self):
        self._figure_data = {}

        for name, figfunc in self._figure_funcs.items():
            memfile = io.BytesIO()
            figfunc(self).savefig(memfile, format='png')
            self._figure_data[name] = base64.b64encode(memfile.getvalue()).decode('utf-8')


        self._figout.clear_output()
        for name, data in self._figure_data.items():
            html = "%s<br/><img src=\"data:image/png;base64,%s\" /><br/>" % (name, data)
            self._figout.append_display_data(HTML(html))
            self.print("rendered fig %s" % name)
        




class Data(object):
    def __init__(self, iden=None, dataset=None):
        self._stored = ['iden']
        self.iden = iden
        self.dataset = dataset
    
    def next(self):
        return self.dataset.find_next(self)
        
    def last(self):
        return self.dataset.find_last(self)

    def to_dict(self):
        return {k: getattr(self, k) for k in self._stored}